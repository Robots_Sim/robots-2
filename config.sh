#!/bin/bash

rm -f Makefile



echo "CFLAGS = -std=c++11 -Wall -g" >> Makefile
echo "LDFLAGS = -lc -lm -lglut -lGL" >> Makefile

echo "" >>Makefile
echo "" >>Makefile
echo "" >>Makefile
echo "all: run" >>Makefile
echo "" >>Makefile
echo "" >>Makefile

all=""
objs=""

for file in `ls ./src | grep ".cpp$"`; do
    
    name=${file::-4}
    
    echo "$name: src/$name.cpp src/$name.h"  >> Makefile
    echo -e "\tg++ \$(CFLAGS) -c ./src/$file -o ./obj/$name.o"  >> Makefile
	
	all="$name $all"
	objs="obj/$name.o $objs"

	
	echo "" >>Makefile


done


echo "" >>Makefile
echo "" >>Makefile
echo "" >>Makefile

echo "run: $all " >>Makefile
echo -e "\tg++ -o bin/robots/robots $objs \$(LDFLAGS) ">>Makefile
