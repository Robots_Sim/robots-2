CFLAGS = -std=c++11 -Wall -g
LDFLAGS = -lc -lm -lglut -lGL



all: run


Ball: src/Ball.cpp src/Ball.h
	g++ $(CFLAGS) -c ./src/Ball.cpp -o ./obj/Ball.o

Color: src/Color.cpp src/Color.h
	g++ $(CFLAGS) -c ./src/Color.cpp -o ./obj/Color.o

ConvexPolygon: src/ConvexPolygon.cpp src/ConvexPolygon.h
	g++ $(CFLAGS) -c ./src/ConvexPolygon.cpp -o ./obj/ConvexPolygon.o

Engine: src/Engine.cpp src/Engine.h
	g++ $(CFLAGS) -c ./src/Engine.cpp -o ./obj/Engine.o

Entity: src/Entity.cpp src/Entity.h
	g++ $(CFLAGS) -c ./src/Entity.cpp -o ./obj/Entity.o

KeySet: src/KeySet.cpp src/KeySet.h
	g++ $(CFLAGS) -c ./src/KeySet.cpp -o ./obj/KeySet.o

main: src/main.cpp src/main.h
	g++ $(CFLAGS) -c ./src/main.cpp -o ./obj/main.o

Placeable: src/Placeable.cpp src/Placeable.h
	g++ $(CFLAGS) -c ./src/Placeable.cpp -o ./obj/Placeable.o

Point: src/Point.cpp src/Point.h
	g++ $(CFLAGS) -c ./src/Point.cpp -o ./obj/Point.o

Polygon: src/Polygon.cpp src/Polygon.h
	g++ $(CFLAGS) -c ./src/Polygon.cpp -o ./obj/Polygon.o

Robot: src/Robot.cpp src/Robot.h
	g++ $(CFLAGS) -c ./src/Robot.cpp -o ./obj/Robot.o

Utility: src/Utility.cpp src/Utility.h
	g++ $(CFLAGS) -c ./src/Utility.cpp -o ./obj/Utility.o

Vector2f: src/Vector2f.cpp src/Vector2f.h
	g++ $(CFLAGS) -c ./src/Vector2f.cpp -o ./obj/Vector2f.o

View: src/View.cpp src/View.h
	g++ $(CFLAGS) -c ./src/View.cpp -o ./obj/View.o

Window: src/Window.cpp src/Window.h
	g++ $(CFLAGS) -c ./src/Window.cpp -o ./obj/Window.o




run: Window View Vector2f Utility Robot Polygon Point Placeable main KeySet Entity Engine ConvexPolygon Color Ball  
	g++ -o bin/robots/robots obj/Window.o obj/View.o obj/Vector2f.o obj/Utility.o obj/Robot.o obj/Polygon.o obj/Point.o obj/Placeable.o obj/main.o obj/KeySet.o obj/Entity.o obj/Engine.o obj/ConvexPolygon.o obj/Color.o obj/Ball.o  $(LDFLAGS) 
