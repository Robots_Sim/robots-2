#include "Color.h"
#include "Window.h"
#include "KeySet.h"
#include "Engine.h"


#include <ctime>
#include <cstdlib>




//Maybe some day all of this should go away

void display(void);
void keyDown( unsigned char key, int x, int y );
void keyUp( unsigned char key, int x, int y );
void keySpecialDown( int key, int x, int y );
void keySpecialUp( int key, int x, int y );

//This is a buffer that the engine reads the kyboard input
KeySet * keys = new KeySet();


/**
 * Some things TODO
 * initialize all values in entity
 * 
 */


int main(int argc, char ** argv)
{
    
    
    srand(time(NULL));
    
    
    //start the game engine
    //this should start a new thread
    Engine control = Engine();


    //add some stuff to it

    control.getBall()->place(0.5f,0.5f,0.0f);

    control.addRobot();

   control.getBots()[0]->place(0.3f,0.5f,0.0f);

    control.addRobot();
    control.getBots()[1]->place(0.7f,0.5f,0.0f);



    //initialize the graphical subsystem
    Window window = Window(argc,argv,800,800,keys,control,display,keyDown,keyUp,keySpecialDown,keySpecialUp);


    for (Entity* e : control.getEnties())
        cout << e<<endl;



    /*
    * In this loop is executed the set of all operations that are run by both:
    *
    * 1)the game engine control
    * 2)the rendering system
    *
    */
    while (1)
    {   
        //keyboard inpput
        window.handleEvents();
        
        

        //pass the keyboard input to the game engine
        control.readInput(*keys);


        //temporary, this will be embedded in the thread
        control.mainloop();

        window.sleep();
        
        //opengl calls
        window.updateDisplay();
    }




}







void keyDown( unsigned char key, int x, int y ) {
    keys->normal[key] = true;
}
void keyUp( unsigned char key, int x, int y ) {
    keys->normal[key] = false;
}
void keySpecialDown( int key, int x, int y ) {
    keys->special[key] = true;
}
void keySpecialUp( int key, int x, int y ) {
    keys->special[key] = false;
}
void display(void){};
