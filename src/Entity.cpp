#include "Entity.h"
#include "Utility.h"


#include <cmath>



int Entity::id_counter = 0;

Entity::Entity()
    :ConvexPolygon(),
    color()
{
    id = id_counter++;

    collideble = 1;
    

}


int Entity::getId()const
{
    return id;
}


void Entity::update(double dt)
{

    x += vx*dt;
    y += vy*dt;
    a += va*dt;


}

void Entity::setVelocity(double vx_, double vy_, double va_){

    vx = vx_;
    vy = vy_;
    va = va_;

}

void Entity::increaseForwardVelocity(double dv){

    vx += cos(a)*dv;
    vy += sin(a)*dv;

}

void Entity::rotateClockwise(double dv){
    va -= dv;

}


double  Entity::getVx()const
{
    return vx;
}

double  Entity::getVy()const
{
    return vy;
}

double Entity::getVa()const
{
    return va;
}


double Entity::getM()const
{
    return m;
}

double Entity::getI()const
{
    return i;
}

Color Entity::getColor() const
{   
    return color;
}





void  Entity::draw()const
{
    drawPolygon( this->getAbsoluteVertices() , this->getColor().getFloats() );
}


//friend ostream& operator<<( ostream &os, Entity &V );





