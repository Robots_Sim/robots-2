#include "Robot.h"
#include <cmath>

using namespace std;

const float r = 0.10f;
//half of the small angle formed bty the diagonals
const float beta = 1.1;


Robot::Robot()
    :Entity()
{
    x = 0;
    y = 0;
    vx = 0;
    vy = 0;
    a = 0;
    va = 0;


    // Here is where the drawing of the robot occurs

    vertices.push_back({x = 0.07  , y =  0.03});
    vertices.push_back({x = 0.07  , y = -0.03});
    vertices.push_back({x = -0.07 , y = -0.03});
    vertices.push_back({x = -0.07 , y =  0.03});





}

Robot::~Robot()
{
    //dtor
}
