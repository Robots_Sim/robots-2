#ifndef UTILITY_H
#define UTILITY_H

#include <GL/freeglut.h>
#include <cmath>
#include <vector>

#include "Vector2f.h"

/**

**This file contains general utility-functions to aid with the developement.

**/

// round a float value to certain number of decimal places:
double round( double value, int precision );

// Converts given angle in degrees to radians:
double toRadian( double a );

// Converts given angle in radians to degrees:
double toDegree( double a );

// Return whether the value 'v' is between 'a' and 'b'
bool isNotBetween( double v, double a, double b );


/* Drawing functions */

  // Draw a straight line between point 'a' and 'b' in the specified color:
  void drawLine( Point a, Point b, float *color );

  // Draw the given vector:
  void drawVector( Vector &V, float *color );

  // Draw a triangle with specified corners in the specified color:
  void drawTriangle( Point a, Point b, Point c, float *color );

  // Draw a rectangle from one corner at point 'a' to the opposite corner at point 'b' in the specified color:
  void drawRectangle( Point a, Point b, float *color );


  // Draw a polygon with given four points:
  void drawPolygon( Point a, Point b, Point c, Point d, float *color );

  // Draw polygon from given list of points:
  void drawPolygon( const std::vector<Point> &point_list, float *color );

  // Draw a single square point of the specified color:
  void drawPoint( Point a, float *color );
  
  
  
#endif
