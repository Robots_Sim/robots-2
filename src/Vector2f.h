#ifndef VECTOR2F_H
#define VECTOR2F_H

#include <cmath>
#include "Point.h"

/**

**Vectors to be used with physics processing.

* Creates a 2D vector for quantities like velocity and accelation.

* Overloaded for output stream and operators: VectorAddition[ + - ], ScalarMultiplication[ / * ].


**/

class Vector {

  // Returns standard angle of the line formed by given lenghts:
  float getLineAngle( float dx, float dy );

  // Returns standard angle of the line formed by given points:
  float getLineAngle( const Point &a, const Point &b );

  public:

    Point origin; // base of the vector.

    float x; // x-component of the vector.
    float y; // y-component of the vector.

    float angle; // standard angle of the vector.

    float size; // magnitude of the vector.

    Vector();

    // Copy constructor:
    Vector( const Vector &V );

    // Creates a vector at point (a,b) with given magnitude in the given direction;
    Vector( float a, float b, float magnitude, float a_degree );

    // Creates a vector from point a to point b:
    Vector( const Point &a, const Point &b );

    // Updates all the members based on given vector:
    void update( const Vector &V );

    // Translates the base of the vector:
    void setOrigin( float x, float y );

    // Returns the size of vector:
    float getSize( void );

    // Change the size of the vector;
    void setSize( float magnitude );

    // Slides the vector along given angle 'a' by 's' units
    void slideAlong( float a_radian, float s );

    // Change the direction by 'da' degrees:
    void changeDirection( float da_radian );

    // Flips the current direction:
    void invert( void );

    // Returns the end point of the vector:
    Point getEndPoint( void );


    /*Overloaded Operators*/

    Vector operator+( const Vector &B ); // Returns sum of vectors *this and B;
    Vector& operator+=( const float ds ); // Modifies the size of vector by 'ds' units.

    Vector operator-( const Vector &B ); // Returns sum of vectors *this and -B;
    Vector& operator-=( const float ds ); // Modifies the size of vector by '-ds' units.


    Vector& operator/=( const float c ); // Divide size by 'c' units.
    Vector& operator*=( const float c ); // Multiply size by 'c' units.

    friend ostream& operator<<( ostream &os, Vector &V );

};
#endif
