#ifndef ROBOT_H
#define ROBOT_H

#include "Entity.h"
/**
*
* At the moment does nothing, entity does all the work.
*
* TODO: This class will:
*
*   1)Write the vertices
*   2)Turn moving command into motion by calls to entity methods
*   3) do robot specific stff like grabbing the ball
*
*
*/
class Robot:public Entity
{
    public:
        Robot();
        ~Robot();


};

#endif // ROBOT_H
