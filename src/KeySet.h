#ifndef KEY_SET_H
#define KEY_SET_H

#include <GL/freeglut.h> // Special key constants
#include <array>

using namespace std;

/**

**Stores a list of keyboard inputs as booleans;
  Normal refers to keys with ASCII representation;
  Special contains keys like arrow keys, refer to the table at the end.

* Overloaded for output stream;

**/

class KeySet {

  public:

    array<bool,128> normal; // Array of ASCII chars, 127 is the last index.
    array<bool,109> special ; // Array of SPECIAL keys, 108 is the last index.

    KeySet();

    ~KeySet();

    // Returns true if the given 'key' from 'normal' array is pressed:
    bool normalIsDown( char key );

    // Returns true if the given 'key' from 'normal' array is not pressed:
    bool normalIsUp( char key );

    // Returns true if the given 'key' from 'special' array is pressed:
    bool specialIsDown( char key );

    // Returns true if the given 'key' from 'special' array is not pressed:
    bool specialIsUp( char key );


    // Additional functions to simplify code:

    // Returns whether specified arrow is pressed:
    bool arrowIsDown( char key );

    // Returns whether specified arrow is pressed:
    bool arrowIsUp( char key );


  friend ostream& operator<<( ostream &os, KeySet &keys );

};

/*

SPECIAL Key Constants

GLUT_KEY_F1            :           F1 function key.                (1)
GLUT_KEY_F2            :           F2 function key.                (2)
GLUT_KEY_F3            :           F3 function key.                (3)
GLUT_KEY_F4            :           F4 function key.                (4)
GLUT_KEY_F5            :           F5 function key.                (5)
GLUT_KEY_F6            :           F6 function key.                (6)
GLUT_KEY_F7            :           F7 function key.                (7)
GLUT_KEY_F8            :           F8 function key.                (8)
GLUT_KEY_F9            :           F9 function key.                (9)
GLUT_KEY_F10           :           F10 function key.               (10)
GLUT_KEY_F11           :           F11 function key.               (11)
GLUT_KEY_F12           :           F12 function key.               (12)
GLUT_KEY_LEFT          :           Left directional key.           (100)
GLUT_KEY_UP            :           Up directional key.             (101)
GLUT_KEY_RIGHT         :           Right directional key.          (102)
GLUT_KEY_DOWN          :           Down directional key.           (103)
GLUT_KEY_PAGE_UP       :           Page up directional key.        (104)
GLUT_KEY_PAGE_DOWN     :           Page down directional key.      (105)
GLUT_KEY_HOME          :           Home directional key.           (106)
GLUT_KEY_END           :           End directional key.            (107)
GLUT_KEY_INSERT        :           Inset directional key.          (108)

*/
#endif
