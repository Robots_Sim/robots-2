#include "KeySet.h"

#include <iostream>

KeySet::KeySet() {

  //  init to zero
  normal = {0};
  special = {0};

}

KeySet::~KeySet() {
    
}

bool KeySet::normalIsDown( char key ) {

    // type cast makes gcc happy
  return normal[(int)key] == true;

}

bool KeySet::normalIsUp( char key ) {

  return normal[(int)key] == false;

}

bool KeySet::specialIsDown( char key ) {

  return special[(int)key] == true;

}

bool KeySet::specialIsUp( char key ) {

  return special[(int)key] == false;

}

bool KeySet::arrowIsDown( char key ) {

  switch ( key ) {

    case 'u':
    return special[GLUT_KEY_UP] == true;
    case 'd':
    return special[GLUT_KEY_DOWN] == true;
    case 'l':
    return special[GLUT_KEY_LEFT] == true;
    case 'r':
    return special[GLUT_KEY_RIGHT] == true;
    default:
    return 0;
    break;// throw InvalidArrow exception

  }

}


bool KeySet::arrowIsUp( char key ) {

  return !arrowIsUp(key);

}

ostream& operator<<( ostream &os, KeySet &keys ) {

  // Print all pressed keys from normal set:
  cout << "Normal: \n";

  for (int i = 0; i < 128; i++)

    if ( keys.normal[i] ) cout << i << " (" << (char) i << ")\n";


  // Print all pressed keys from special set:
  cout << "Special: \n";

  for (int i = 0; i < 109; i++) {

    if ( keys.special[i] ) cout << i << endl;

    if ( i == 12 ) i = 99; // Jump to 100 in next iteration, check Special Key table in header file for reference.
  }

  return os;

}
