#include "Ball.h"

#include <cmath>
#include <vector>

using namespace std;

Ball::Ball()
    :Entity()
{
    x = 0.5;
    y = 0.5;
    a = 0;



   double twicePi = M_PI * 2.0;

    for(int i = 0; i < edge_resolution; i++)
    {
        vertices.push_back(Point( radius * cos( twicePi * (double) i / edge_resolution) ,
                                  radius * sin( twicePi * (double) i / edge_resolution) ));

    }


}

Ball::~Ball()
{
    //dtor
}
