#ifndef POLYGON_H
#define POLYGON_H

#include <vector>

#include "Placeable.h"
#include "Point.h"

using namespace std;


/**
* Handles collisions and inherits position storage and handling
*
* its a convex polygon because concave polygons are way harder to handle
*/
class ConvexPolygon: public Placeable
{
public:

    /**
    *
    *
    */
    ConvexPolygon();
    

    virtual ~ConvexPolygon();

    bool containsVertexOf(const ConvexPolygon &p) const;

    vector<Point> getRelativeVertices() const;
    vector<Point> getAbsoluteVertices() const;


    unsigned countVertices() const;

protected:

    /**
    * the position of the vertices relative to the centre of mass
    *
    * these vertices form a closed path where the first is
    * connected to the last, that represents the perimeter of the
    * polygon
    *
    *
    *
    *
    */
    std::vector<Point > vertices;


};

#endif // POLYGON_H
