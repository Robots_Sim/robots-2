#ifndef PLACEABLE_H
#define PLACEABLE_H



typedef struct
{
    double x;
    double y;
    double a;
} xya_t;



/**
*
* Class that hadles position storage and placing
*
*/
class Placeable
{
public:
    Placeable();
    Placeable(const Placeable &p);

    void place(double x_,double y_, double a_);
    void place(const xya_t &pos);
    void place(const Placeable &p);

    xya_t  getPos()const;

    double getX()const;
    double getY()const;
    double getA()const;


protected:

    double x;
    double y;
    double a; //radians



};

#endif // PLACEABLE_H
