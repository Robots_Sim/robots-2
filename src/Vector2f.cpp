#include "Vector2f.h"
#include "Utility.h"

Vector::Vector() {

  origin = Point(0,0);
  x = 0;
  y = 0;
  angle = 0;
  size = 0;

}

Vector::Vector( const Vector &V ) {

  origin = V.origin;
  x = V.x;
  y = V.y;
  angle = V.angle;
  size = V.size;

}

Vector::Vector( float a, float b, float magnitude, float a_degree ) {

  origin = Point( a, b );

  angle = toRadian(a_degree);

  size = magnitude;

  x = size * cos(angle);
  y = size * sin(angle);

}

Vector::Vector( const Point &a, const Point &b ) {

  origin = Point( a.x, a.y );

  x = b.x - a.x;
  y = b.y - a.y;

  size = sqrt( x*x + y*y );

  angle = getLineAngle( x, y );

}

void Vector::update( const Vector &V ) {

  origin.x = V.origin.x;
  origin.y = V.origin.y;

  x = V.x;
  y = V.y;

  angle = V.angle;

  size = V.size;

}

void Vector::setOrigin( float x, float y ) {

  origin.x = x;
  origin.y = y;

}

float Vector::getSize( void ) {

  return size;

}

void Vector::setSize( float magnitude ) {

  size = magnitude;

  x = size * cos(angle);
  y = size * sin(angle);

}

void Vector::slideAlong( float a_radian, float s ) {

  origin.slideAlong( a_radian, s );

}

void Vector::changeDirection( float da_radian ) {

  angle += da_radian;

  x = size * cos(angle);
  y = size * sin(angle);

}

void Vector::invert( void ) {

  x = -x;
  y = -y;

  angle += M_PI;

}

Point Vector::getEndPoint() {

  Point p( origin.x + x, origin.y + y );

  return p;

}

float Vector::getLineAngle( float dx, float dy ) {

  return (dy > 0) ? acos( dx / sqrt(dx*dx + dy*dy) ) : -acos( dx / sqrt(dx*dx + dy*dy) );

}

float Vector::getLineAngle( const Point &a, const Point &b ) {

  return getLineAngle( b.x - a.x, b.y - a.y );

}


/*Overloaded Operators*/

Vector Vector::operator+( const Vector &B ) {

  Vector V( *this ); // Create a copy of current vector.

  V.x += B.x;
  V.y += B.y;

  V.angle = getLineAngle( origin,  V.getEndPoint() );
  V.size = sqrt( V.x*V.x + V.y*V.y );

  return V;

}
Vector& Vector::operator+=( const float ds ) {

  size += ds;

  x = size * cos(angle);
  y = size * sin(angle);

  return *this;

}

Vector Vector::operator-( const Vector &B ) {

  Vector V( *this ); // Create a copy of current vector.

  V.x -= B.x;
  V.y -= B.y;

  angle = getLineAngle( origin,  V.getEndPoint() );
  size = sqrt( V.x*V.x + V.y*V.y );

  return V;

}
Vector& Vector::operator-=( const float ds ) {

  size -= ds;

  x = size * cos(angle);
  y = size * sin(angle);

  return *this;

}


Vector& Vector::operator/=( const float c ) {

  size /= c;

  x = size * cos(angle);
  y = size * sin(angle);

  return *this;

}
Vector& Vector::operator*=( const float c ) {

  size *= c;

  x = size * cos(angle);
  y = size * sin(angle);

  return *this;

}

ostream& operator<<( ostream &os, Vector &V ) {

  os << V.origin << " to " << "( " << V.origin.x + V.x << ", " << V.origin.y + V.y << " )\n";
  
  return os;

}
