#ifndef BALL_H
#define BALL_H

#include "Entity.h"



/**
*
* TODO: Add a property called height that is used to determine whether
* the ball is above or below robots
*
*/
class  Ball: public Entity
{
public:
    static constexpr double radius = 0.03f;
    static constexpr int edge_resolution = 20;

    Ball();
    virtual ~Ball();


};

#endif // BALL_H
