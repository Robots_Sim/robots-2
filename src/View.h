#ifndef VIEW_H_INCLUDED
#define VIEW_H_INCLUDED

#include "ConvexPolygon.h"

#include "Placeable.h"

/**
*
* This is an interafce that all things that have to be drawn on the screen
* shoud inherit
*
*/
class View
{
public:

    virtual void draw() const = 0;


};


#endif // VIEW_H_INCLUDED


