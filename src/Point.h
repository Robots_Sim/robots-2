#ifndef POINT_H
#define POINT_H

#include <iostream>
#include <cmath>

using namespace std;

/**

**Points to be used for defining position of vectors and objects.

* Overloaded for output stream.

* Implementation in the same file because its a trivial class.

**/

class Point {

  public:

    double x;
    double y;

    Point();

    // Creates a new point at (a,b):
    Point( double a, double b );

    // Move to the given location:
    void move( double a, double b );

    // Slide the point in the direction of 'a' by 's' units:
    void slideAlong( double a_radian, double s );


    // Overloaded output stream:
    friend ostream& operator<<( ostream &os, Point &point );

};

#endif
