#ifndef ENGINE_H
#define ENGINE_H


#include "KeySet.h"
#include "Robot.h"
#include "Ball.h"

#include <vector>
#include <ctime>


/**
*
* This class represents a game engine that controls the physiscs of the game
*
* The AI will provide this class with instructions to send to the robots, but
* at the moment not being there any AI it handles human control.
*
* TODO add a time scaling factor to allow for slowm motion and maybe implement
* virtual timing to allow for deterministic games.
*/
class Engine
{

private:


    // entities
    Ball* ball = new Ball();
    std::vector<Robot*> bots;


    //Stores e
    std::vector<Entity*> entities;


    // timing
    clock_t prev_loop;
    clock_t new_loop;
    double dt;//seconds



public:
    Engine();
    ~Engine();



    /// The actual physics simulator operates within this function
    void mainloop();


    ///Updates the keys that are currently pressed at a given time
    void readInput(const KeySet &keys);


    //Entity operations

    Ball* getBall();

    void addRobot();



    std::vector<Robot*> getBots(); //TODO change to accessBots (since is non const)
    
    std::vector<Entity*> getEnties(); //same here

};

#endif // ENGINE_H
