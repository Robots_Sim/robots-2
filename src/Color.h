#ifndef COLOR_H
#define COLOR_H

/**
* simple class that uses Ritchar's libary to make pretty colors
*
* don't call if gui init has not been called!
*
* TODO: add detection for GUI::Init call
*/
class Color {

  public:

    float all[4]; //values between 0 and 1

    Color();
    // Color( int r_val, int g_val, int b_val, float o_val );

    // Color( float r_val, float g_val, float b_val, float o_val );

    void changeColor( int r_val, int g_val, int b_val, float o_val );
    
    float *getFloats() const;


};


#endif // COLOR_H
