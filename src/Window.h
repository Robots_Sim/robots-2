#ifndef WINDOW_H
#define WINDOW_H

#include <GL/freeglut.h> //Interface library
#include <unistd.h>
#include <cstdint>


#include "KeySet.h"
#include "Engine.h"



/**
*
* This class controls the user-interaction and graphic elements of the game (Through pointers of KeySet and Entity class)
*
*/
class Window {

private: 
    unsigned int width, height;

    const size_t FRAME_TIME = 2e4;

    KeySet *keys;

    Engine control;//read only reference
    
    Color background;

public:

    Window( void );
    ~Window( void );

    ///Initialize constructure:
 
    Window( int argc, char** argv,
                unsigned int new_width,
                unsigned int new_height,
                KeySet * key_set,
                const Engine & controlP,
                void (*display)(void),
                void (*keyDown)(unsigned char, int, int),
                void (*keyUp)(unsigned char, int, int),
                void (*keySpecialDown)(int, int, int),
                void (*keySpecialUp)(int, int, int));

    /// Updates KeySet booleans and handles redraw requests:
    void handleEvents( void );


    /// Renders the current graphics state onto the screen based on member's data:
    void updateDisplay( void );


    /*Display utility-functions*/


    /// Clear the window to the default background color:
    void clear( void );

    /// Change background to given color;
    void setBackground( const float color[4] );


    /// Delays the next instruction by default-time units:
    void sleep( void );

};

#endif
