#include "Point.h"

Point::Point() {
  x = 0;
  y = 0;

}

Point::Point( double a, double b ) {
  x = a;
  y = b;

}

void Point::move( double a, double b ) {
  x = a;
  y = b;

}

void Point::slideAlong( double a_radian, double s ) {

  x += s * cos(a_radian);
  y += s * sin(a_radian);

}

ostream& operator<<( ostream &os, Point &point ) {

  os << "( " << point.x << ", " << point.y << " ) ";
  return os;

}
