#include "Engine.h"

#include "Robot.h"
#include "Ball.h"
#include "KeySet.h"

#include <iostream>

#include <ctime>

using namespace std;

void collide(Entity *a, Entity *b);


Engine::Engine()
    :bots()
    ,entities()
{

    entities.push_back(ball);

    prev_loop = clock();


}

Engine::~Engine()
{
    delete ball;

    for(Robot* r: bots){
        delete r;
    }

}

/**
* This function is only designited for human interactions with the program
*
* When the AI will be fully implemented this class will not comunicate in any
* direct way with the entities
*
*/
void Engine::readInput(const KeySet &keys)
{
    if(keys.normal[(int)'w']){
        bots[0]->increaseForwardVelocity(0.03);
    }

    if(keys.normal[(int)'s']){
        bots[0]->increaseForwardVelocity(-0.03);
    }

    if(keys.normal[(int)'d']){
        bots[0]->rotateClockwise(0.08);
    }

     if(keys.normal[(int)'a']){
        bots[0]->rotateClockwise(-0.08);
    }



}


/**
*
* This is the where the actual physiscs calculations will take place.
*
*
* The AI will be implemented separately from the physics engine.
*/

void Engine::mainloop()
{

    //Physics


    // Collision handling

    for(Entity* e : entities){
        for(Entity* f: entities){
            if (f->containsVertexOf(*e)){
                collide(e,f);

            }

        }
    }





    //get intervall time and store it in dt

    new_loop = clock();

    dt = (double (new_loop-prev_loop)/CLOCKS_PER_SEC);

    prev_loop = new_loop;




    ball->update(dt);

    for(Robot * b: bots){
        b->update(dt);
    }



}


void Engine::addRobot(){

    bots.push_back(new Robot());
    entities.push_back(bots.back());
}



Ball* Engine::getBall()
{
    return ball;
}


vector<Robot*> Engine::getBots()
{
    return bots;
}

void collide(Entity* a, Entity* b ){

    cout<<"collision between entities of id: "<<a->getId()
        <<" and: "<<b->getId()<<endl;


}

std::vector<Entity *> Engine::getEnties()
{   
    return entities;
}

