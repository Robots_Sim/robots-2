#include "Utility.h"



double round( double value, int precision ) {
    const int adjustment = pow(10,precision);
    return floor( value*(adjustment) + 0.5 )/adjustment;
}

double toRadian( double a ) {

  return a * M_PI/180;

}
double toDegree( double a ) {

  return a * 180/M_PI;

}

bool isNotBetween( double v, double a, double b ) {

  return ( v <= a || v >= b );

}

/*Drawing functions*/

void drawPoint( Point a, float *color ) {

  glColor4fv( color );
  glBegin( GL_POINTS );
    glVertex2d( a.x, a.y );
  glEnd();

}

void drawLine( Point a, Point b, float *color ) {

  glColor4fv(color);
  glBegin(GL_LINES);
    glVertex2d( a.x, a.y );
    glVertex2d( b.x, b.y );
  glEnd();

}

void drawVector( Vector &V, float *color ) {

  glColor4fv(color);
  glLineWidth( 3 );
  glBegin(GL_LINES);
    glVertex2d( V.origin.x, V.origin.y );
    glVertex2d( V.origin.x + V.x, V.origin.y +  V.y );
  glEnd();

  // Draw arrows at endpoints:
  Vector A( V ); A -= 0.02;
  A.changeDirection(0.05); Point a = A.getEndPoint();
  A.changeDirection(-0.1); Point b = A.getEndPoint();
  float black_color[4] = {0,0,0,0.5};

  drawTriangle( a, b, V.getEndPoint(), black_color );

}

void drawTriangle( Point a, Point b, Point c, float *color ) {

  glColor4fv( color );
  glBegin(GL_TRIANGLES);
    glVertex2d( a.x, a.y );
    glVertex2d( b.x, b.y );
    glVertex2d( c.x, c.y );
  glEnd();

  const float dark = 0.2;
  glColor4f( dark * color[0], dark * color[1], dark * color[2], 1.0 );

  glLineWidth(2);
  glBegin(GL_LINE_LOOP);
    glVertex2d( a.x, a.y );
    glVertex2d( b.x, b.y );
    glVertex2d( c.x, c.y );
  glEnd();

}

void drawRectangle( Point a, Point b, float *color ) {

  glColor4fv(color);
  glRectf( a.x, a.y, b.x, b.y );

  const float dark = 0.2;
  glColor4f( dark * color[0], dark * color[1], dark * color[2], 1.0 );

  glLineWidth(2);
  glBegin(GL_LINE_LOOP);
    glVertex2d( a.x, a.y );
    glVertex2d( b.x, a.y );
    glVertex2d( b.x, b.y );
    glVertex2d( a.x, b.y );
  glEnd();

}

void drawPolygon( Point a, Point b, Point c, Point d, float *color ) {


  glColor4fv(color);

  glBegin(GL_POLYGON);
    glVertex2d( a.x , a.y );
    glVertex2d( b.x , b.y );
    glVertex2d( c.x , c.y );
    glVertex2d( d.x , d.y );
  glEnd();

}

// Draw polygon from given list of Points.
void drawPolygon( const vector<Point> &point_list, float *color ) {

  glColor4fv(color);

  glBegin(GL_POLYGON);
    for(auto point: point_list)
      glVertex2d( point.x , point.y );
  glEnd();

}
