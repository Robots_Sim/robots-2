#include "Placeable.h"

Placeable::Placeable()
    :x(0)
    ,y(0)
    ,a(0)
{
}

Placeable::Placeable(const Placeable &p){
    x = p.getX();
    y = p.getY();
    a = p.getA();
}

void Placeable::place(double x_,double y_, double a_)
{

    x = x_;
    y = y_;
    a = a_;

}


void Placeable::place(const xya_t &xya)
{

    x = xya.x;
    y = xya.y;
    a = xya.a;

}


void Placeable::place(const Placeable &p){

    x = p.x;
    y = p.y;
    a = p.a;

}



xya_t Placeable:: getPos()const
{

    xya_t xya;

    xya.x = x;
    xya.y = y;
    xya.a = a;

    return xya;

}

double Placeable::getX()const
{
    return x;
}
double Placeable::getY()const
{
    return y;
}
double Placeable::getA()const
{
    return a;
}
