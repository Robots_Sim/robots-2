#include "Window.h"
#include "Utility.h"
#include "Color.h"

#include <cstdlib>

Window::Window(void) {
    width = 0;
    height = 0;
}

Window::~Window( void ) {


}



/* TODO
     * 
     * implemet draw
     * remove unnecessary classes
     * fix color generation
     * make sure no methods are missing in window ... should be done
     * write a main file
     */




Window::Window( int argc, char** argv,
                unsigned int new_width,
                unsigned int new_height,
                KeySet * key_set,
                const Engine &controlP,
                void (*display)(void),
                void (*keyDown)(unsigned char, int, int),
                void (*keyUp)(unsigned char, int, int),
                void (*keySpecialDown)(int, int, int),
                void (*keySpecialUp)(int, int, int)
              )
    :background()
{
    
    
    //TODO remove function arguments

    width = new_width;
    height = new_height;

    keys = key_set;

        control = controlP;

    // Configure a window and display context.
    glutInit(&argc, argv);
    glutInitDisplayMode( GLUT_SINGLE | GLUT_RGBA );
    glutInitWindowSize( width, height );
    glutCreateWindow( argv[0] );
    glEnable(GL_BLEND);
    glEnable(GL_LINE_SMOOTH);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    glutIgnoreKeyRepeat( 1 ); // Disable key repeat
    glPointSize(4);

    // Set the projection matrix for a 2D top-down view.
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);

    // Install callbacks if supplied
    if( display )
        glutDisplayFunc( display );

    if( keyDown )
        glutKeyboardFunc( keyDown ); // Normal ASCII Key down event.

    if( keyUp )
        glutKeyboardUpFunc( keyUp ); // Normal ASCII Key up event.

    if ( keySpecialDown )
        glutSpecialFunc( keySpecialDown ); // Special Key down event.

    if ( keySpecialUp )
        glutSpecialUpFunc( keySpecialUp ); // Special Key up event.
    
    
    glClearColor( background.all[0], background.all[1], background.all[2], background.all[3] ); // Set new color.
    glClear( GL_COLOR_BUFFER_BIT );

}



void Window::handleEvents() {

    glutMainLoopEvent();
    
    if ( keys->normal[(int)'q'] ) {

        cout << "Quitting...\n";
        exit(1);

    }


}


void Window::updateDisplay() {

    clear();
    
    std::vector<Entity*> ents = control.getEnties();
    
    
    
    for(unsigned i = 0; i< ents.size(); i++)
        ents[i]->draw();
    

    glFlush();
    
    
    glutPostRedisplay();

}


/*Display utility functions*/

void Window::clear() {
    glClearColor( background.all[0]+0.1, background.all[1], background.all[2], background.all[3] ); // Set new color.
    glClear( GL_COLOR_BUFFER_BIT ); // Clear screen.


}

void Window::setBackground( const float color[4] ) {
    
    background.changeColor(color[0],color[1],color[2],color[3]);
    
    
}



void Window::sleep() {

    usleep(FRAME_TIME);

}
