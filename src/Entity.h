#ifndef ENTITY_H
#define ENTITY_H

#include "ConvexPolygon.h"
#include "View.h"
#include "Color.h"



/**
*
* Class that represents any massive entity in the game
* It has some basic control functions that allow for placing and moving
*
* Position information storage is inherited from Placeble, superclass of
* Convex polygon
*
* Each entity should have a unique id
*
* TODO: add a functions that converts force into acceleration
*/

class Entity : public ConvexPolygon, View
{

private:
    int id;
    static int id_counter;


protected:
    bool collideble = true;//unused

    double vx = 0;
    double vy = 0;
    double va = 0; //angular velocity

    double m = 1; //mass
    double i = 1; //moment of inertia
    
    
    Color color;







public:

    double getVx()const;
    double getVy()const;
    double getVa()const;

    void setVelocity(double vx, double vy, double va);
    void increaseForwardVelocity(double dv);
    void rotateClockwise(double da);

    double getM()const;
    double getI()const;
    
    
    Color getColor()const;



    Entity();

    int getId()const;

    void update(double usecs);
    
    void draw()const override;








};

#endif // ENTITY_H
