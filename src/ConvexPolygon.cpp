#include "ConvexPolygon.h"

#include <cmath>

using namespace std;

ConvexPolygon::ConvexPolygon()
    :Placeable()
    ,vertices()
{
}




ConvexPolygon::~ConvexPolygon()
{
}


unsigned ConvexPolygon::countVertices()const{
    return vertices.size();
}




/////////////////////// BEGIN COLLISION DETECTION ///////////////////////////


typedef struct {

    Point a;
    Point b;
    Point c;
}triangle_t;






bool static inline triangle_contains(const triangle_t t, const Point v){

    return (
            ((v.x - t.a.x)/(t.b.x-t.a.x)-(v.y - t.a.y)/(t.b.y-t.a.y)) *
            ((t.c.x-t.a.x)/(t.b.x-t.a.x)-(t.c.y-t.a.y)/(t.b.y-t.a.y))
            >0

            ) && (

            ((v.x - t.b.x)/(t.c.x-t.b.x)-(v.y - t.b.y)/(t.c.y-t.b.y)) *
            ((t.a.x-t.b.x)/(t.c.x-t.b.x)-(t.a.y-t.b.y)/(t.c.y-t.b.y))
            >0

            ) && (

            ((v.x - t.c.x)/(t.a.x-t.c.x)-(v.y - t.c.y)/(t.a.y-t.c.y)) *
            ((t.b.x-t.c.x)/(t.a.x-t.c.x)-(t.b.y-t.c.y)/(t.a.y-t.c.y))
            >0

            );

}




//TODO find a better algorithm
bool ConvexPolygon::containsVertexOf(const ConvexPolygon &p)const {

    if( vertices.size()<=2 || p.countVertices()<=2)
        return false;


    triangle_t t;
    #define NEXT(x) ((x)+1>=vertices.size() ? 0 : i+1)


    for(unsigned i = 0; i<vertices.size(); i+= 2){
        t.a = this->getAbsoluteVertices()[i];
        t.b = this->getAbsoluteVertices()[NEXT(i)]; //NEXT only works on vertices
        t.c = this->getAbsoluteVertices()[NEXT(NEXT(i))];

        for(unsigned j = 0; j<p.countVertices(); j++){
            if( triangle_contains( t, (p.getAbsoluteVertices())[j] ) )//inline
                return true;
        }

    }
    #undef NEXT



    return false;


}


/////////////////////// END COLLISION DETECTION ///////////////////////////






vector<Point> ConvexPolygon::getRelativeVertices() const{

    return vertices;
 }


vector<Point> ConvexPolygon::getAbsoluteVertices() const{

    vector<Point > out = vector<Point>(vertices.size());

    for (unsigned i = 0; i<out.size(); i++){
        out[i].x = x + vertices[i].y*sin(a) + vertices[i].x*cos(a);
        out[i].y = y + vertices[i].x*sin(a) - vertices[i].y*cos(a);
    }

    return out;
 }


